echo ""
cat misc/title.txt

echo ""
echo "===================================================================="
ls
echo ""
read -r -p "Please enter the name of the .sh file used to configure the credentials: " fileconfig


source $(echo "${fileconfig}")


if [ -a user_data.txt ]
then 
 rm user_data.txt
fi

echo ""
echo "===================================================================="
read -r -p "Please enter the Password to be set for the server: " TF_VAR_password_input
export TF_VAR_password=$TF_VAR_password_input
#******************************************* Set the server username
export TF_VAR_username="ubuntu"

#******************************************* Set the server hostname
echo ""
echo "===================================================================="
read -r -p "Please enter the name to be set for the server: " TF_VAR_hostname_input
export TF_VAR_hostname=$TF_VAR_hostname_input

#******************************************* Set the ssh key name
echo ""
echo "===================================================================="
read -r -p "Please enter the name of the ssh key that will be generated: " TF_VAR_sshkey_input
export TF_VAR_sshkey=$TF_VAR_sshkey_input

#******************************************* Set the ssh key name
echo ""
echo "===================================================================="
read -r -p "Please enter the zerotier network ID to join: " TF_VAR_zerotier_input
export TF_VAR_zerotier=$TF_VAR_zerotier_input

#******************************************* Set the ssh key name
echo ""
echo "===================================================================="
openstack image list
echo ""
read -r -p "Please enter the name of the image you want to use: " TF_VAR_image
export TF_VAR_image

#******************************************* Set the ssh key name
echo ""
echo "===================================================================="
echo "Available flavors:"
echo ""
echo "m2.medium     (CPUs=2, RAM=4Gb, disk=20Gb)"
echo "------------------------------------------"
echo "m2.large      (CPUs=4, RAM=8Gb, disk=40Gb)-recommended"
echo ""
read -r -p "Please enter the flavour of the instance (m2.medium/m2.large): " TF_VAR_flavor
export TF_VAR_flavor

echo ""
echo "===================================================================="
echo "Done!"

#****************************************** Create the conf file user_data.txt
echo "#cloud-config">>user_data.txt
echo "password: ${TF_VAR_password}">>user_data.txt
echo "chpasswd: { expire: False }">>user_data.txt
echo "ssh_pwauth: True">>user_data.txt
