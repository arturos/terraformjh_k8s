#compute the key to associate to the VM

resource "openstack_compute_keypair_v2" "test-keypair" {
  name = var.sshkey
}

#=====================================================================================================

#setup of the VM instance considering the specific parameters of openstack cern
resource "openstack_compute_instance_v2" "test1" { 					
						  							
  name            = var.hostname
  image_name      = var.image
  flavor_name     = var.flavor
  key_pair        = "${openstack_compute_keypair_v2.test-keypair.name}"
  security_groups = ["default"]
  #set the server to allow remote access with custom password (psw config performed during the setup)
  user_data       = "${file("user_data.txt")}"   							

  
#do something in the local terraform directory on ur pc
#provisioner "local-exec" {       									
#   command = <<EOT
#     echo ciao;
#     mkdir soap
#  EOT

 #do something in the instance remote directory
 provisioner "remote-exec" {       									
    inline = [
#	 "sudo touch /boot/grub/menu.lst",
#	 "sudo update-grub2",
#	 "sudo apt update",
#	 "sudo apt --yes upgrade",
#	 "sudo apt update",
    "echo ==========================",
    "echo Installing Zerotier",
    "echo ==========================",
    "curl -s https://install.zerotier.com | sudo bash",
    "sudo zerotier-cli join ${var.zerotier}",
    "echo ==========================",
    "echo Installing Dependencies",
    "echo ==========================",
    #install the prerequisites (option always yes activated)
    "sudo apt --yes install python3 python3-dev git curl python-pip",                                             
    "sudo apt-get --yes install dpkg-dev cmake g++ gcc binutils libx11-dev libxpm-dev libxft-dev libxext-dev libssl-dev",
    "echo ==========================",
    "echo Installing TLJH",
    "echo ==========================",
    #install TLJH
    "sudo curl -L https://tljh.jupyter.org/bootstrap.py | sudo -E python3 - --admin ubuntu",   	
    "echo ==========================",
    "echo Installing ROOT",
    "echo ==========================",
    #clone root rep
    "git clone --branch v6-22-00-patches https://github.com/root-project/root.git root_src",	
    "mkdir builddir installdir",
    "cd builddir",
    "cmake -DCMAKE_INSTALL_PREFIX=../installdir -DPYTHON_EXECUTABLE=/opt/tljh/user/bin/python3.7 ../root_src",
    # INSTALL ROOT
    "sudo cmake --build . --target install -- -j4",							
    "cd ..",
    "echo ''>>.bashrc",
    "echo 'alias python3=/opt/tljh/user/bin/python3.7'>>.bashrc",
    "echo ''>>.profile",
    #link the .sh file in the .profile
    "echo 'source builddir/bin/thisroot.sh'>>.profile",
    "echo ''>>.profile",
    #add user authentication
    "sudo tljh-config set auth.type nativeauthenticator.NativeAuthenticator",	
    "sudo tljh-config set auth.NativeAuthenticator.open_signup true",
    #import env vars into TLJH
    "sudo su <<EOF",
    "echo 'c.Spawner.environment = {'>>/opt/tljh/config/jupyterhub_config.d/environment.py",	
    "echo '        \"PYTHONPATH\": \"/home/ubuntu/builddir/lib\",'>>/opt/tljh/config/jupyterhub_config.d/environment.py",
    "echo '        \"LD_LIBRARY_PATH\": \"/home/ubuntu/builddir/lib\"'>>/opt/tljh/config/jupyterhub_config.d/environment.py",
    "echo '}'>>/opt/tljh/config/jupyterhub_config.d/environment.py",
    "tljh-config reload"													
        ]

  #set the type of connection with the user and the psw previoulsy set
  connection {												
    type     = "ssh"
    user     = var.username
    password = var.password
    host     = "${openstack_compute_instance_v2.test1.access_ip_v4}"
  }  
 }
}
