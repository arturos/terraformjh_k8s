# Terraform deployer for JupyterHub+ROOT

This repository provides a step-by-step guide to deploy in a simple and fast way a virtual instance with JupyterHub and ROOT running on it.

***Disclaimer***: for users who want to exploit the resources of CERN (e.g. Openstack@CERN), it is mandatory to act from the inside of the CERN's subnet by either accessing LXPLUS-CLOUD via the command 
```
ssh user@lxplus-cloud.cern.ch
``` 
or using a machine which is already in the subnet. [I don't have SUDO privileges on LXPLUS](#12.1).


1. [Install Terraform](#1)
    - [Terraform Installation for LXPLUS-CLOUD Users](#2) <br>
2. [Deploy an instance using Openstack@CERN](#3)
    - [Install OpenStack CLI](#4)
        - [Installing with conda (recommended)](#4.2)
        - [Installing with pip](#4.1)
    - [Cloning the repo and configuring the credentials using the OpenStack RC file](#6)
    - [Create an Image with OpenStack CLI](#5)
    - [Deploy the instance through Terraform](#7)
        - [(Optional) Setting up ZeroTier](#6.1)
3. [Deploy an instance using Amazon Web Services (AWS)](#8)
    - [Install AWS CLI](#9)
    - [Creating a key-pair](#10)
    - [Cloning repository and configuring credentials](#11)
4. [Basic Troubleshooting](#12)
    - [Create an independent VM to perform the deployement operations](#12.1)


## Install Terraform <a name="1"></a>
The main step to achieve is to properly install **Terraform**. As explained [here](https://learn.hashicorp.com/tutorials/terraform/install-cli):
1. Add the HashiCorp [GPG key](https://apt.releases.hashicorp.com/gpg).
```
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
```
2. Add the official HashiCorp Linux repository.
```   
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
```
3. Update and install.
```    
sudo apt-get update && sudo apt-get install terraform
```
To verify the installation, just type `terraform version` in the terminal.
#### Terraform Installation for LXPLUS-CLOUD Users <a name="2"></a>
For LXPLUS-CLOUD users, the Terraform commands can be made available by downloading the binaries from [here](https://www.terraform.io/downloads.html), unzipping it and moving it to the directory in which all the operations will be performed. <br> 
To download the LXPLUS Terraform binary run 
```
wget https://releases.hashicorp.com/terraform/0.13.5/terraform_0.13.5_linux_amd64.zip
```
Then unzip it doing 
```
unzip terraform_0.13.5_linux_amd64.zip
```
A single file named "terraform" should appear in your directory. <br>
From the directory in which the binary is, you will be able to run terraform. To verify the executable you just downloaded, just type `./terraform` in the terminal.
Moreover, it is possible to verify which version of Terraform is installed by typing `./terraform version`, and making sure it is seccessive to v0.13.
You can use the command `./terraform 0.13upgrade .` if the installed version is outdated. <a name="tf_update"></a>

It is nevertheless **highly recommended** to install Terraform following the steps listed in the section (SUDO privileges required) [above](#1).

## Deploy an instance using Openstack@CERN <a name="3"></a>

### Install OpenStack CLI <a name="4"></a>
In order to deploy a VM, a proper image file is needed. The most straightforward way to create a custom image in OpenStack is from the CLI. 

For those who use the `lxplus-cloud.cern.ch` connection, a functioning version of Openstack CLI is already installed and ready to be used. <br>
For other users it will be necessary to follow the instructions below.

#### Installing with conda (recommended) <a name="4.2"></a>
1. First download the conda installation file:
```
wget https://repo.anaconda.com/archive/Anaconda3-2020.07-Linux-x86_64.sh
```
2. Then execute it:
```
bash Anaconda-latest-Linux-x86_64.sh
```
3. Follow the prompts on the installer screens. If you are unsure about any setting, accept the defaults. You can change them later. <br>
**To make the changes take effect, close and then re-open your terminal window**.

4. Test your installation. In your terminal window or Anaconda Prompt, run the command `conda list`. A list of installed packages appears if it has been installed correctly.
5. To install openstack with conda run one of the following:
```
sudo conda install -c conda-forge python-openstackclient
```
```
sudo conda install -c conda-forge/label/cf202003 python-openstackclient 
```
To verify the installation, run `openstack --help`.

#### Installing with pip <a name="4.1"></a>
To install the CLI with `pip`, follow these 2 simple steps: 
1. Make sure all the prerequisites are installed:

| Prerequisite        | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Python 2.7 or later | Currently, the clients do not support Python 3.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| setuptools package  | Installed by default on Mac OS X. Many Linux distributions provide packages to make setuptools easy to install. Search your package manager for setuptools to find an installation package.  If you cannot find one, download the setuptools package directly from https://pypi.python.org/pypi/setuptools.  The recommended way to install setuptools on Microsoft Windows is to follow the documentation provided on the setuptools website (https://pypi.python.org/pypi/setuptools).  Another option is to use the unofficial binary installer maintained by Christoph Gohlke (http://www.lfd.uci.edu/~gohlke/pythonlibs/#setuptools). |
| pip package         | To install the clients on a Linux, Mac OS X, or Microsoft Windows system, use pip.  It is easy to use, ensures that you get the latest version of the clients from the Python Package Index, and lets you update or remove the packages later on.  <br> **MacOS**: ```easy_install pip ``` <br><br>  **Windows**: Ensure that the `C:\Python27\Scripts` directory is defined in the `PATH` environment variable, and use the `easy_install` command from the setuptools package: ```C:\>easy_install pip``` <br><br> **Ubuntu/Debian**: ``` apt install python-dev python-pip  ```                                                                                                                                                                                                                                                                                                                                      |

2. Execute 
```
pip install python-openstackclient
```

#### Cloning the repo and configuring the credentials using the OpenStack RC file <a name="6"></a>

First, clone the repository using 
```
git clone https://gitlab.cern.ch/gguerrie/terraformjh_k8s.git
```
enter in the "openstack_deployer_VM_tljh_root" folder and make sure the following files are present:
- setup.sh
- providers.tf
- main.tf
- variables.tf

In order to deploy instances on Openstack, it is necessary to get the RC.sh file from the web interface. This operation ca be achieved following these steps:
1. Log in to the dashboard and from the drop-down list select the project for which you want to download the OpenStack RC file.
2. On the **Project** tab, open the **Compute** tab and click **Tools** on the upper right corner of the page.
3. Click **OpenStack RC File v3** and save the file ([This](https://openstack.cern.ch/project/api_access/openrc/) is the direct download link).
4. Copy the **OpenStack RC File v3** file to the computer from which you want to run OpenStack commands.
5. Rename the file as "`openrc.sh`"

The "`openrc.sh`" file has to be located in the "TLJH+ROOT (VM) on Openstack@CERN" directory. <br>

From a terminal inside the created directory execute 
```
bash
source openrc.sh
``` 
and insert the information requested; this passage will initialize the env variables used by Terraform. <br>
Again, for Openstack@CERN users, the following steps can only be achieved from inside the CERN subnet (e.g. accessing LXPLUS-CLOUD).



#### Create an Image with OpenStack CLI <a name="5"></a>

If you do not have the image of the OS you want to install, you can use openstack to build your own image. First, download the image:
```
wget https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img
```
As an example, an Ubuntu 18.04 server image is downloaded with the command above. All the Ubuntu server distributions can be inspected at [this page](https://cloud-images.ubuntu.com/).

To create an image, use openstack **image create**; the following list explains the optional arguments that you can use with the create and set commands to modify image properties. 
<br> For more information, refer to the [OpenStack Image command reference](https://docs.openstack.org/developer/python-openstackclient/command-objects/image.html).
Remember that the `setup.sh` file or the `openrc.sh` file have to be sourced ***before*** lauching the image creation command. 
```
 openstack image create --disk-format qcow2 --container-format bare --shared --file bionic-server-cloudimg-amd64.img bionic-server
```
- `--disk-format qcow2` refers to the format used by openstack to deploy the image, in this case qemu.
- `--container-format bare` refers to the image container format. The supported options are: ami, ari, aki, bare, docker, ova, ovf. The default format is: bare.
- `--shared` refers to the sharability of the image once created. 
- `-file bionic-server-cloudimg-amd64.img` provides the path to the image file.
- `bionic-server` is the name with which the image will be saved on Openstack.

Additional informations can be found [here](https://docs.openstack.org/python-openstackclient/pike/cli/command-objects/image.html). <br>
This operation can take several minutes. Once finished, the list of images can be accessed by running:
```
openstack image list
```

### Deploy the instance through Terraform <a name="7"></a>

Before launching the instance, it is mandatory to configure properly Terraform. In order to do so, from inside the cloned directory execute
```
bash
source setup.sh
```
Some key parameters are required to complete the configuration. Below all the necessary attributes are listed:
- the `.sh` file to be used for the provider authentication (e.g. `openrc.sh`).
- The password to be set for the VM remote login.
- The name to be assigned to the VM.
- The name of the ssh security key that will be created for the VM.
- The ZeroTier network ID that must be used (leave black if no networks are needed).
- The image of the OS that the VM will install.
- The flavor of the instance, i.e. the number of cores and the size of the memory of the instance. It is highly recommended to deploy a `m2.large` machine, in order to optimize the subsequent installation processes.

From the same terminal initialize the project, which downloads a plugin that allows Terraform to interact with the provider. <br>
**If Terraform was "installed" via binaries, all the follwing commands must be executed as** `./terraform [some command]`. 
```
terraform init
```
As mentioned [here](#tf_update), it could be necessary to upgrade the current version of the software. Moreover, if a warning similar to the one below appears:
```
Warning: Interpolation-only expressions are deprecated

  on main.tf line 15, in resource "openstack_compute_instance_v2" "test1":
  15:   key_pair        = "${openstack_compute_keypair_v2.test-keypair.name}"

Terraform 0.11 and earlier required all non-constant expressions to be
provided via interpolation syntax, but this pattern is now deprecated. To
silence this warning, remove the "${ sequence from the start and the }"
sequence from the end of this expression, leaving just the inner expression.

Template interpolation syntax is still used to construct strings from
expressions when the template includes multiple interpolation sequences or a
mixture of literal strings and interpolations. This deprecation applies only
to templates that consist entirely of a single interpolation sequence.

```
Nothing serious is actually going on, just a little reminder about the commands syntax in the previous versions of the software. <br>
The rest of the Terraform output should look like this:
```
Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

```
Then proceed executing 
```
terraform plan
```
Here Terraform performs a refresh, unless explicitly disabled, and then determines what actions are necessary to achieve the desired state specified in the configuration files. Finally launch
```
terraform apply
```
which is used to apply the changes required to reach the desired state of the configuration, or the pre-determined set of actions generated by a terraform plan execution plan.
Once the process is terminated, you can access via ssh to the instance by running
```
ssh ubuntu@my-instance-IP
```
and providing the password you chose during the configuration process. For Openstack@CERN users, unless already acting from inside the CERN subnet, the IP must be the one provided on the [ZeroTier network page](https://my.zerotier.com/network/) and corresponding to the instance.


##### (Optional) Setting up ZeroTier <a name="6.1"></a>
[ZeroTier](https://www.zerotier.com/) provides open-source software, SDKs and commercial products and services to create and manage virtual software defined networks. 
For our particular case, ZeroTier will provide a subnet that combines the capabilities of VPN and SD-WAN, simplifying network management for users not inside the CERN subnet. <br>
This resource will allow the user to be able to remoltely connect to the instance (after it is created).
<br> <br>To download and install ZeroTier on your pc, create a free account and follow the instructions reported in [this](https://www.zerotier.com/download/) page.
Then access [this page](https://my.zerotier.com/network) and create your own private subnet; for our purposes, the default template that the system propose is perfectly fine.
<br>To join the created network, run from the CLI
```
zerotier-cli join ################
```
with ############### being the 16-digit network ID of the network you wish to join. On UNIX based OSes, this requires sudo. On Windows, this requires an administrator command prompt.
<br>Finally, make sure that the joining device is validated, checking it in the "Members" section in the network dashboard online. 

## Deploy an instance using Amazon Web Services (AWS) <a name="8"></a>
Other major providers, as Amazon Web Services, can be used to achieve the goal; below, a step-by-step tutorial for the AWS provider is presented.
#### Install AWS CLI on Linux <a name="9"></a>
This section describes how to install, update, and remove the AWS CLI version 2 on Linux. The AWS CLI version 2 has no dependencies on other Python packages. It has a self-contained, embedded copy of Python included in the installer. 

1. Download the installation file in one of the following ways:
    - **Use the `curl` command**: 
    ``` 
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" 
    ```
    - **Downloading from the URL**: To download the installer with your browser, use one of the following URLs. You can [verify](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html#v2-install-linux-validate) the integrity and authenticity of your downloaded installation file before you extract (unzip) the package.
    For the latest version of AWS: [https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip](https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip). <br>
2. Unzip the installer. If your Linux distribution doesn't have a built-in `unzip` command, use an equivalent to unzip it. The following example command unzips the package and creates a directory named `aws` under the current directory. 
``` 
unzip awscliv2.zip 
```
3. Run the install program. The installation command uses a file named `install` in the newly unzipped `aws` directory. By default, the files are all installed to `/usr/local/aws-cli`, and a symbolic link is created in `/usr/local/bin`. The command includes `sudo` to grant write permissions to those directories. 
``` 
sudo ./aws/install 
```
4. To verify the installation, run 
```
aws --version
```

For additional information, visit [the official installation tutorial page](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html).


#### Creating a key-pair <a name="10"></a>
To guarantee the security of your connection, a key pair is necessary. A key-pair can be easily created from the AWS CLI by running:
```
aws ec2 create-key-pair --key-name MyKeyPair --query 'KeyMaterial' --output text > MyKeyPair.pem
```
The resulting `MyKeyPair.pem` file looks similar to the following. 
```
-----BEGIN RSA PRIVATE KEY-----
EXAMPLEKEYKCAQEAy7WZhaDsrA1W3mRlQtvhwyORRX8gnxgDAfRt/gx42kWXsT4rXE/b5CpSgie/
vBoU7jLxx92pNHoFnByP+Dc21eyyz6CvjTmWA0JwfWiW5/akH7iO5dSrvC7dQkW2duV5QuUdE0QW
Z/aNxMniGQE6XAgfwlnXVBwrerrQo+ZWQeqiUwwMkuEbLeJFLhMCvYURpUMSC1oehm449ilx9X1F
G50TCFeOzfl8dqqCP6GzbPaIjiU19xX/azOR9V+tpUOzEL+wmXnZt3/nHPQ5xvD2OJH67km6SuPW
oPzev/D8V+x4+bHthfSjR9Y7DvQFjfBVwHXigBdtZcU2/wei8D/HYwIDAQABAoIBAGZ1kaEvnrqu
/uler7vgIn5m7lN5LKw4hJLAIW6tUT/fzvtcHK0SkbQCQXuriHmQ2MQyJX/0kn2NfjLV/ufGxbL1
mb5qwMGUnEpJaZD6QSSs3kICLwWUYUiGfc0uiSbmJoap/GTLU0W5Mfcv36PaBUNy5p53V6G7hXb2
bahyWyJNfjLe4M86yd2YK3V2CmK+X/BOsShnJ36+hjrXPPWmV3N9zEmCdJjA+K15DYmhm/tJWSD9
81oGk9TopEp7CkIfatEATyyZiVqoRq6k64iuM9JkA3OzdXzMQexXVJ1TLZVEH0E7bhlY9d8O1ozR
oQs/FiZNAx2iijCWyv0lpjE73+kCgYEA9mZtyhkHkFDpwrSM1APaL8oNAbbjwEy7Z5Mqfql+lIp1
YkriL0DbLXlvRAH+yHPRit2hHOjtUNZh4Axv+cpg09qbUI3+43eEy24B7G/Uh+GTfbjsXsOxQx/x
p9otyVwc7hsQ5TA5PZb+mvkJ5OBEKzet9XcKwONBYELGhnEPe7cCgYEA06Vgov6YHleHui9kHuws
ayav0elc5zkxjF9nfHFJRry21R1trw2Vdpn+9g481URrpzWVOEihvm+xTtmaZlSp//lkq75XDwnU
WA8gkn6O3QE3fq2yN98BURsAKdJfJ5RL1HvGQvTe10HLYYXpJnEkHv+Unl2ajLivWUt5pbBrKbUC
gYBjbO+OZk0sCcpZ29sbzjYjpIddErySIyRX5gV2uNQwAjLdp9PfN295yQ+BxMBXiIycWVQiw0bH
oMo7yykABY7Ozd5wQewBQ4AdSlWSX4nGDtsiFxWiI5sKuAAeOCbTosy1s8w8fxoJ5Tz1sdoxNeGs
Arq6Wv/G16zQuAE9zK9vvwKBgF+09VI/1wJBirsDGz9whVWfFPrTkJNvJZzYt69qezxlsjgFKshy
WBhd4xHZtmCqpBPlAymEjr/TOlbxyARmXMnIOWIAnNXMGB4KGSyl1mzSVAoQ+fqR+cJ3d0dyPl1j
jjb0Ed/NY8frlNDxAVHE8BSkdsx2f6ELEyBKJSRr9snRAoGAMrTwYneXzvTskF/S5Fyu0iOegLDa
NWUH38v/nDCgEpIXD5Hn3qAEcju1IjmbwlvtW+nY2jVhv7UGd8MjwUTNGItdb6nsYqM2asrnF3qS
VRkAKKKYeGjkpUfVTrW0YFjXkfcrR/V+QFL5OndHAKJXjW7a4ejJLncTzmZSpYzwApc=
-----END RSA PRIVATE KEY-----
```
Your private key isn't stored in AWS and can be retrieved ***only*** when it's created. You can't recover it later. Instead, if you lose the private key, you must create a new key pair.

If you're connecting to your instance from a Linux computer, it is recommended that you use the following command to set the permissions of your private key file so that only you can read it. 
```
chmod 400 MyKeyPair.pem
```

#### Cloning repository and configuring credentials <a name="11"></a>
With your account created (remember, you need to create an account), the CLI installed and the key-pair available, you can now clone the repository named "TLJH+ROOT (VM) on AWS". Make sure you have at least the following files:
- setup.sh
- main.tf
- variables.tf

To initiate the infrastructure configuration, from inside the cloned folder run:
```
source setup.sh
```
Then follow the prompts to input your AWS Access Key ID and Secret Access Key, which you'll find on [this page](https://console.aws.amazon.com/iam/home?#security_credential). 
Additional informations are required, as the region or the output format; the latter is normally the `json` format, while a full list of the available regions is reported [here](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-regions).
Finally, some more parameters are required to complete the configuration. Below all the necessary attributes are listed:
- AWS Access Key ID
- Secret Access Key
- Region
- Output format
- Password for the ssh remote access to the instance
- Private ssh-key file location
- Name of the created ssh-public-key for the instance
- Region again (?)
- Name of the created security group for the instance

To deploy the instance through Terraform, see [this section](#7).


## Basic Troubleshooting <a name="12"></a>

#### Create an independent VM to perform the deployement operations <a name="12.1"></a>
Since the majority of the operations are performed with root privileges, it would be optimal to have a machine with such characteristics. <br>
One of the easiest options is to create a VM on Opestack@CERN that can perform the operations described in this guide.

To easily create a VM with Openstack, go on the [webpage](https://openstack.cern.ch/) and click on Compute->Instances in the left side lateral bar. Then click on "LAUCH INSTANCE". 





