provider "aws" {
	region = var.region
}


resource "aws_key_pair" "ssh-key" {
  key_name   = var.sshkeyname
  public_key = var.sshkey
}


#=====================================================================================================

#setup of the VM instance considering the specific parameters of AWS
resource "aws_instance" "example" {
  ami           = "ami-09a1e275e350acf38"
  instance_type = "t2.micro"		
  security_groups    = ["${aws_security_group.example.name}"]
  key_name        = "${aws_key_pair.ssh-key.key_name}"		
  associate_public_ip_address = true
  user_data = "${file("user_data.txt")}"

 provisioner "remote-exec" {       									#do something in the instance remote directory
    inline = [
#	 "sudo touch /boot/grub/menu.lst",
#	 "sudo update-grub2",
#	 "sudo apt update",
#	 "sudo apt --yes upgrade",
#	 "sudo apt update",
         "echo ==========================",
	 "echo Installing Dependencies...",
         "echo ==========================",
	 "sudo apt --yes install python3 python3-dev git curl python-pip",                                         #install the prerequisites (option always yes activated)    
	 "sudo apt-get --yes install dpkg-dev cmake g++ gcc binutils libx11-dev libxpm-dev libxft-dev libxext-dev libssl-dev",
	 "sudo curl -L https://tljh.jupyter.org/bootstrap.py | sudo -E python3 - --admin ubuntu",   	#install TLJH
         "echo ==========================",
	 "echo Installing ROOT...",
         "echo ==========================",
	 "git clone --branch v6-22-00-patches https://github.com/root-project/root.git root_src",	#clone root rep
	 "mkdir builddir installdir",
	 "cd builddir",
	 "cmake -DCMAKE_INSTALL_PREFIX=../installdir -DPYTHON_EXECUTABLE=/opt/tljh/user/bin/python3.7 ../root_src",
	 "sudo cmake --build . --target install -- -j4",							# INSTALL ROOT
	 "source root/bin/thisroot.sh # also available: thisroot.{csh,fish,bat}",			#execute the .sh init file
	 "cd ..",
	 "echo ''>>~/.bashrc",
         "echo 'alias python3=/opt/tljh/user/bin/python3.7'>>~/.bashrc",
	 "echo ''>>~/.profile",										#link the .sh file in the .profile
	 "echo 'source builddir/bin/thisroot.sh'>>~/.profile",
	 "echo ''>>~/.profile",
         "echo ==========================",
	 "echo Adding admins...",
         "echo ==========================",
	 "sudo tljh-config add-item users.admin soap",
         "echo ==========================",
	 "echo Reloading configuration...",
         "echo ==========================",
	 "sudo tljh-config reload",
	 "echo ==========================",
	 "echo Accessing as SU...",
         "echo ==========================",
	 "sudo su <<EOF",
	 "echo 'c.Spawner.environment = {'>>/opt/tljh/config/jupyterhub_config.d/environment.py",	#import env vars into TLJH
	 "echo '        \"PYTHONPATH\": \"/home/ubuntu/builddir/lib\",'>>/opt/tljh/config/jupyterhub_config.d/environment.py",
      	 "echo '        \"LD_LIBRARY_PATH\": \"/home/ubuntu/builddir/lib\"'>>/opt/tljh/config/jupyterhub_config.d/environment.py",
	 "echo '}'>>/opt/tljh/config/jupyterhub_config.d/environment.py",
	 "tljh-config reload"										
        ]

  connection {												#set the type of connection with the user and the psw previoulsy set (DA MIGLIORARE)
    type     = "ssh"
    user     = var.username
    password = var.password
    host     = "${aws_instance.example.public_ip}"
  }  
 }


}
resource "aws_security_group" "example" {
  name        = var.security_group
  description = "ssh"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
 }


output "instance_ip_addr" {
  value = aws_instance.example.public_ip
}
