variable "username" {
  description = "The username for the DB master user"
  type        = string
}

variable "password" {
  description = "The password for the DB master user"
  type        = string
}

variable "security_group" {
  description = "The security group of the VM created in aws"
  type        = string
}

variable "sshkey" {
  description = "The VM's public sshkey created in aws"
  type        = string
}

variable "sshkeyname" {
  description = "The VM's public sshkey name created in aws"
  type        = string
}

variable "region" {
  description = "The name of the region"
  type        = string
}


