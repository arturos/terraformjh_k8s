echo ""
cat misc/title.txt
echo "Configuring the AWS provider..."
/usr/local/bin/aws configure

if [ -a user_data.txt ]
then 
 rm user_data.txt
fi

echo ""
read -rs -p "Please enter the Password to be set for the remote access to the instance: " TF_VAR_password
export TF_VAR_password
#******************************************* Set the server username

echo "Default usernameset to \"ubuntu\" "
export TF_VAR_username="ubuntu"


#******************************************* Set the ssh key name 
ls
echo ""
read -r -p "Please enter the path to the private ssh key that will be used: " TF_VAR_sshkey_input
TF_VAR_sshkey1=`ssh-keygen -y -f $TF_VAR_sshkey_input` 
echo "Your public key is: " 
echo "$TF_VAR_sshkey1"
export TF_VAR_sshkey=$(echo "$TF_VAR_sshkey1")


#******************************************* Set the ssh key name
echo ""
read -r -p "Please enter the ssh-key name for this instance: " TF_VAR_sshkeyname
export TF_VAR_sshkeyname


#******************************************* Set the ssh key name
echo ""
read -r -p "Please enter the region: " TF_VAR_region
export TF_VAR_region

#******************************************* Set the security group name
echo ""
read -r -p "Please enter the security group name: " TF_VAR_security_group
export TF_VAR_security_group

#****************************************** Create the conf file user_data.txt
echo "#cloud-config">>user_data.txt
echo "password: ${TF_VAR_password}">>user_data.txt
echo "chpasswd: { expire: False }">>user_data.txt
echo "ssh_pwauth: True">>user_data.txt
